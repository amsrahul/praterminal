# Perhitungan sederhana dengan javascript
file `hitung.js` memiliki fungsi untuk perhitungan sederhana.  

## Persiapan
Sebelum memulai pastikan kamu sudah memiliki Bahan-bahan berikut:
1. Node JS
2. Visual Studio Code
3. WSL (untuk pengguna windows 10/11), git bash (untuk pengguna windows 7/8), terminal untuk Linux/Mac OS

## Menjalankan project
Buka terminal dan masuk ke dalam folder project, kemudian ketik perintah berikut.

```bash
# untuk test penambahan
node test.js tambah

# untuk test pengurangan
node test.js kurang

# untuk test pembagian
node test.js bagi

# untuk test perkalian
node test.js kali
```